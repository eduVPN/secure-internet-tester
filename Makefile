# note: this file is intended for development only and not to actually
#       install the client.
#

.DEFAULT_GOAL := build
.PHONY: venv install-deps install-mypy mypy install-lint fmt lint clean build

VENV := ./venv
RUFF := $(shell command -v ruff 2> /dev/null)
ifeq ("$(wildcard $(RUFF))","")
	RUFF = $(shell echo "${PWD}/venv/bin/ruff")
endif
MYPY := $(shell command -v mypy 2> /dev/null)
ifeq ("$(wildcard $(MYPY))","")
	MYPY = $(shell echo "${PWD}/venv/bin/mypy")
endif

venv:
	python3 -m venv $(VENV)
	$(VENV)/bin/pip install --upgrade pip build

install-deps: venv
	$(VENV)/bin/pip install --index-url "https://test.pypi.org/simple/" eduvpn-common
	$(VENV)/bin/pip install .

install-mypy: venv install-deps
	$(VENV)/bin/pip install ".[mypy]" --no-cache-dir

mypy:
ifeq ("$(wildcard $(MYPY))","")
	@echo "mypy does not exist, install it with make install-mypy (will use pip) or consult your distribution manual"
	exit 1
endif
	$(MYPY) eduvpntester

install-lint: venv
	$(VENV)/bin/pip install ".[lint]"

fmt:
ifeq ("$(wildcard $(RUFF))","")
	@echo "ruff does not exist for formatting, install it with make install-lint (will use pip) or consult your distribution manual"
	exit 1
endif
	$(RUFF) format eduvpntester

lint:
ifeq ("$(wildcard $(RUFF))","")
	@echo "ruff does not exist for linting, install it with make install-lint (will use pip) or consult your distribution manual"
	exit 1
endif
# check linting
	$(RUFF) check eduvpntester
# check formatting
	$(RUFF) format --check eduvpntester

clean:
	rm -rf $(VENV) build dist .eggs eduvpn_secure_internet_tester.egg-info
	find  . -name *.pyc -delete
	find  . -name __pycache__ -delete

build: venv install-deps
	rm -rf build
	rm -rf dist
	$(VENV)/bin/pip install build
	$(VENV)/bin/python3 -m build --sdist --wheel .
