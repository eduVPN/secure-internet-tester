import logging
import subprocess

logger = logging.getLogger(__name__)


class ShellCommandError(Exception):
    def __init__(self, operation: str, stderr: str):
        super().__init__(f"shell command operation: '{operation}' failed: {stderr}")


def run_cmd(cmd: str, operation: str):
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = ""
    assert p.stdout is not None
    for line in p.stdout.readlines():
        output += line.decode("utf-8")
    retval = p.wait()
    logger.debug(f"command finished: '{cmd}', output: {output}")
    if retval != 0:
        raise ShellCommandError(operation, output)
