import json
import logging
from pathlib import Path
from tempfile import TemporaryDirectory

import requests

from requests.adapters import HTTPAdapter, Retry

from eduvpntester import VERSION
from eduvpntester.utils import run_cmd

VPN_NAME = "vpntestcon"

REQUEST_HOST = "https://disco.eduvpn.org/v2/server_list.json"

logger = logging.getLogger(__name__)


class UnknownVPNProtocolError(Exception):
    def __init__(self, protocol: str):
        super().__init__(f"unknown VPN protocol: {protocol}")


class VPNFailedError(Exception):
    def __init__(self, message: str):
        super().__init__(f"VPN connection failed connectivity test: {message}")


class Connection:
    def load(self, config: str):
        protocol = None
        extension = ""
        cfg = json.loads(config)
        if cfg["protocol"] == 1:
            protocol = "openvpn"
            extension = ".ovpn"
        elif cfg["protocol"] == 2:
            protocol = "wireguard"
            extension = ".conf"
        else:
            raise UnknownVPNProtocolError(cfg["protocol"])
        file = f"{VPN_NAME}{extension}"
        with TemporaryDirectory() as tmpdir:
            fp = Path(tmpdir) / file
            with open(fp, "w+") as f:
                f.write(cfg["config"])
            run_cmd(f"nmcli con import type {protocol} file {str(fp)}", "importing VPN file")

    def up(self):
        run_cmd(f"nmcli con up {VPN_NAME}", "setting VPN state to UP")

    def cleanup(self):
        try:
            run_cmd(f"nmcli con delete {VPN_NAME}", "deleting connection")
        except Exception as e:
            logger.debug(f"failed to cleanup VPN connection: {str(e)}")

    def test(self):
        retry_strategy = Retry(
            total=3,
            backoff_factor=1,
        )
        adapter = HTTPAdapter(max_retries=retry_strategy)
        http = requests.Session()
        http.mount("https://", adapter)
        http.mount("http://", adapter)
        headers = {"User-Agent": f"Secure Internet Tester / {VERSION}"}
        try:
            r = http.get(REQUEST_HOST, timeout=10, headers=headers)  # 10 seconds
            if r.status_code < 200 or r.status_code > 299:
                # limit body to 100 characters
                res = r.text[:100]
                if len(r.text) > 100:
                    res += "..."
                raise VPNFailedError(
                    f"Failed discovery request, status code not 2xx (is: {r.status_code}), body: {res}"
                )
        except Exception as e:
            raise VPNFailedError(f"Failed discovery request: {str(e)}")
