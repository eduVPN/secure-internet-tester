import json
import locale
import logging
from pathlib import Path

# stolen from the linux client
COUNTRY_MAP = Path("country_codes.json")
country_mapping = None

logger = logging.getLogger(__name__)


# stolen from the linux client
def _read_country_map() -> dict:
    """
    Read the storage from disk, returns an empty dict in case of failure.
    """
    global country_mapping

    if country_mapping is not None:
        return country_mapping

    if COUNTRY_MAP.exists():
        try:
            with open(COUNTRY_MAP, "r") as f:
                country_mapping = json.load(f)
                return country_mapping
        except Exception as e:
            logger.error(f"Error reading country map: {e}")
    return {}


# stolen from the linux client
def retrieve_country_name(country_code: str) -> str:
    country_map = _read_country_map()
    loc = locale.getlocale()
    if loc[0] is None:
        prefix = "en"
    else:
        prefix = loc[0][:2]
    if country_code in country_map:
        code = country_map[country_code]
        if prefix in code:
            return code[prefix]
    return country_code


def country_output(loc: str) -> str:
    return f"{retrieve_country_name(loc)} ({loc})"
