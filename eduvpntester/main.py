import argparse
import atexit
import json
import logging
import os
import sys
import time
import traceback
import webbrowser
from logging.handlers import SysLogHandler
from pathlib import Path
from typing import Optional

from eduvpn_common.event import class_state_transition
from eduvpn_common.main import EduVPN, ServerType
from eduvpn_common.state import State, StateType

from eduvpntester import VERSION
from eduvpntester.connection import Connection
from eduvpntester.countries import country_output, retrieve_country_name

TOKEN_CACHE = "tokens.json"
STATE_FILE = "tester_state.json"

logger = logging.getLogger(__name__)


class Manager:
    def __init__(self, connection: Connection):
        self.common = EduVPN("org.eduvpn.app.linux", f"{VERSION}+sitester", str(self.data_dir()))
        self.connection = connection
        self.org_id: Optional[str] = None

    @class_state_transition(State.OAUTH_STARTED, StateType.ENTER)
    def oauth_started(self, old_state, data):
        if not self.org_id:
            self.common.cancel()
            logger.error("browser needs to be opened for OAuth")
            return
        logger.warning("browser needs to be opened for OAuth")
        webbrowser.open(data)

    @class_state_transition(State.ASK_PROFILE, StateType.ENTER)
    def ask_profile(self, old_state, profiles):
        profiles_json = json.loads(profiles)
        profile_list = profiles_json["data"]["map"]
        first = ""
        # set first default gateway profile it finds
        # or just the first profile
        for profile in profile_list:
            first = profile
            if profile_list[profile].get("default_gateway", True):
                self.common.cookie_reply(profiles_json["cookie"], profile)
                return
        self.common.cookie_reply(profiles_json["cookie"], first)

    def data_dir(self) -> Path:
        _home = os.path.expanduser("~")
        path = os.getenv("XDG_DATA_HOME")
        if not path:
            path = os.path.join(_home, ".local", ".share")
        path = os.path.join(path, "eduvpntester")
        return Path(path)

    def token_getter(self, server_id: str, server_type: int):
        token_file = self.data_dir() / TOKEN_CACHE
        if not os.path.exists(token_file):
            return None
        with open(token_file, "r") as f:
            data = f.read()
            if data:
                return json.loads(data).get(f"{server_id}+{server_type}", None)
        return None

    def token_setter(self, server_id: str, server_type: int, tokens: str):
        token_loc = self.data_dir()
        os.makedirs(token_loc, exist_ok=True)
        with open(token_loc / TOKEN_CACHE, "w+") as f:
            data = {f"{server_id}+{server_type}": tokens}
            f.write(json.dumps(data))

    def register(self):
        self.common.register()
        self.common.set_token_handler(self.token_getter, self.token_setter)
        self.common.register_class_callbacks(self)

    def add_secure_internet(self, org_id: str):
        self.org_id = org_id
        # update discovery
        self.common.get_disco_organizations()
        self.common.get_disco_servers()

        servers = json.loads(self.common.get_servers())
        if "secure_internet_server" in servers.keys():
            self.common.remove_server(ServerType.SECURE_INTERNET, servers["secure_internet_server"]["identifier"])

        self.common.add_server(ServerType.SECURE_INTERNET, org_id)

    def start_server(self, server, loc: str):
        self.common.set_secure_location(server["identifier"], loc)
        config = self.common.get_config(
            ServerType.SECURE_INTERNET,
            server["identifier"],
            prefer_tcp=False,
            startup=False,
        )

        # load the config into a VPN connection
        self.connection.load(config)

        # ensure the VPN is up
        self.connection.up()

        # test the connection
        self.connection.test()

    def write_state(self, failed, succeeded):
        data_dir = self.data_dir()
        with open(data_dir / STATE_FILE, "w+") as f:
            f.write(
                json.dumps(
                    {
                        "succeeded": succeeded,
                        "failed": failed,
                    }
                )
            )

    def is_state_changed(self, failed, succeeded):
        data_dir = self.data_dir()
        prev_state_file = data_dir / STATE_FILE

        # previous state does not exist
        if not os.path.exists(prev_state_file):
            return True

        # state exists
        with open(prev_state_file, "r") as f:
            data = f.read()
            if not data:
                return True
            data_json = json.loads(data)
            prev_suc = data_json.get("succeeded", [])
            prev_fail = data_json.get("failed", [])

            return set(succeeded.keys()) != set(prev_suc.keys()) or set(failed.keys()) != set(prev_fail.keys())
        return False

    def test_servers(self):
        servers = json.loads(self.common.get_servers())
        if "secure_internet_server" not in servers:
            logger.error("please add a server first using the 'authorize' command")
            sys.exit(1)
        server = servers["secure_internet_server"]
        succeeded = {}
        failed = {}
        sorted_locs = sorted(server["locations"], key=lambda k: retrieve_country_name(k))
        for loc in sorted_locs:
            logger.info(f"Testing location: {country_output(loc)}")
            try:
                self.start_server(server, loc)
            except Exception as e:
                failed[loc] = str(e)
            else:
                succeeded[loc] = ""
            self.connection.cleanup()
            time.sleep(5)

        self.write_state(failed, succeeded)
        if not self.is_state_changed(failed, succeeded):
            logger.info("State remained the same")


def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    traceback.print_exception(exc_type, exc_value, exc_traceback)
    logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


def main():
    # log to syslog
    logger.setLevel(logging.DEBUG)
    handler = SysLogHandler(address="/dev/log")
    handler.setLevel(logging.DEBUG)
    errhand = logging.StreamHandler()
    errhand.setLevel(logging.INFO)
    logger.addHandler(handler)
    logger.addHandler(errhand)

    # log unhandled exceptions
    sys.excepthook = handle_exception

    # Setup connection object
    connection = Connection()

    man = Manager(connection)

    # setup argument parsing
    parser = argparse.ArgumentParser("eduVPN secure internet tester")
    parser.set_defaults(func=lambda _: parser.print_usage())
    subparsers = parser.add_subparsers(title="subcommands")
    authorize_parser = subparsers.add_parser("authorize", help="Add a secure internet server and authorize it")
    authorize_parser.add_argument(
        "-o", "--org-id", type=str, help="The organization ID to add a secure internet server", required=True
    )
    authorize_parser.set_defaults(func=lambda args: man.add_secure_internet(vars(args).get("org_id")))
    check_parser = subparsers.add_parser("check", help="Check all secure internet servers")
    check_parser.set_defaults(func=lambda _: man.test_servers())
    parsed = parser.parse_args()

    connection.cleanup()
    atexit.register(lambda: connection.cleanup())

    man.register()
    parsed.func(parsed)


if __name__ == "__main__":
    main()
