# eduVPN secure internet tester

This script tests all eduVPN secure internet servers for connectivity or OAuth issues.

# Why

eduVPN secure internet servers can break, which is super annoying for users that select the broken location. There can be all sorts of issues: DNS, firewall, OAuth guest access, etc. This script:

- Gets an OpenVPN and WireGuard config for each secure internet location (`https://disco.eduvpn.org/v2/server_list.json`)
- Connect to the VPN using `nmcli`
- Try to curl `disco.eduvpn.org` with a timeout of 10 seconds

# Dependencies

- [python](https://www.python.org/) >= 3.6
- [eduvpn-common](https://github.com/eduvpn/eduvpn-common) >= 1.99.1
- [python3-requests](https://github.com/psf/requests)
- [NetworkManager](https://networkmanager.dev/)

## Optional

- Type checking: [Mypy](https://github.com/python/mypy)
- Other Linting, formatting: [Ruff](https://github.com/astral-sh/ruff)
- Following build instructions: [GNU Make](https://www.gnu.org/software/make/)

# Building

```bash
make
```

Then the Python program gets installed in the virtual env in

```bash
venv/bin/eduvpn-si-tester
```

Wheel files for Pip are written to `dist/eduvpn_secure_internet_tester-*-py3-none-any.whl`. If you install this with pip, it will place the Python program in `~/.local/bin`

# Usage

## Initial authorization
To run this script you first need to obtain valid tokens. This is done using the "authorize" command. This needs your organization ID (`--org-id`), e.g.: 

```bash
eduvpn-si-tester authorize --org-id https://idp.geant.org
```

Tokens are stored in `$XDG_DATA_HOME/eduvpntester/`, if you want to run on a server, copy this dir and then continue with checking. You will have to re-run this every time the VPN expires (every 90 days).

## Checking all secure internet servers

```bash
eduvpn-si-tester check
```

This prints a report to standard output. As logs are written to standard error, you can only print the report with:

```bash
eduvpn-si-tester check 2>/dev/null
```

If the locations that are failing and succeeding are the same as the previous run, it will print nothing.

# Log

All logs are written to Syslog.
Warning and error logs are also written to standard error.

# User agent

The requests of this client have the following user agent:

For requests made by eduvpn-common:
```
org.eduvpn.app.linux/version+sitester eduvpn-common/commonversion
```

and

For requests made from this client directly:
```
Secure Internet Tester / version
```

where `version` and `commonversion` is the version of this client and the version of eduvpn-common respectively.

# Development

## Linting

Run

```bash
make lint
```

## Formatting

Run

```bash
make fmt
```

## Mypy (type checking)

Run

```bash
make mypy
```

## Running the code directly

You can also run the code directly without rebuilding. If you have the venv set up (default `make` or `make venv`), you can run:

```bash
venv/bin/python3 -m eduvpntester.main
```

# License
MIT
